package com.pikecape.springbootrestapihibernate.service;

import com.pikecape.springbootrestapihibernate.exceptions.ResourceNotFoundException;
import com.pikecape.springbootrestapihibernate.model.Duck;
import com.pikecape.springbootrestapihibernate.repository.DuckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DuckService
{
    private final DuckRepository duckRepository;

    @Autowired
    public DuckService(DuckRepository duckRepository) {
        this.duckRepository = duckRepository;
    }

    public List<Duck> getAllDucks()
    {
        return duckRepository.findAll();
    }

    public Duck getDuck(UUID id) throws ResourceNotFoundException
    {
        Duck duck = duckRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Duck " + id + " not found."));
        //return ResponseEntity.ok().body(duck);
        return duck;
    }

    public Duck createDuck(Duck duck)
    {
        // TODO add error handling.
        return duckRepository.save(duck);
    }

    public Duck updateDuck(UUID id, Duck duck) throws ResourceNotFoundException
    {
        Duck storedDuck = duckRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Duck " + id + " not found."));

        // TODO update the who response body; duck.
        storedDuck.setName(duck.getName());

        final Duck updatedDuck = duckRepository.save(storedDuck);
        return updatedDuck;
    }

    public Boolean deleteDuck(UUID id) throws ResourceNotFoundException
    {
        Duck storedDuck = duckRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Duck " + id + " not found."));

        // TODO add error handling.
        duckRepository.delete(storedDuck);

        return true;
    }
}

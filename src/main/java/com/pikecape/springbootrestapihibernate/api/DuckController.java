package com.pikecape.springbootrestapihibernate.api;

import com.pikecape.springbootrestapihibernate.repository.DuckRepository;
import com.pikecape.springbootrestapihibernate.exceptions.ResourceNotFoundException;
import com.pikecape.springbootrestapihibernate.model.Duck;
import com.pikecape.springbootrestapihibernate.service.DuckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/ducks")
public class DuckController
{
    private final DuckService duckService;

    @Autowired
    public DuckController(DuckService duckService)
    {
        this.duckService = duckService;
    }

    @GetMapping()
    public List<Duck> getAllDucks()
    {
        return duckService.getAllDucks();
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Duck> getDuck(@PathVariable(value = "id") UUID id) throws ResourceNotFoundException
    {
        return ResponseEntity.ok().body(duckService.getDuck(id));
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Duck> createDuck(@Valid @NotNull @RequestBody Duck duck)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(duckService.createDuck(duck));
    }

    @PutMapping(path = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Duck> updateDuck(@PathVariable(value = "id") UUID id, @Valid @NotNull @RequestBody Duck duck) throws Exception
    {
        return ResponseEntity.status(HttpStatus.OK).body(duckService.updateDuck(id, duck));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Boolean> deleteDuck(@PathVariable(value = "id") UUID id) throws ResourceNotFoundException
    {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(duckService.deleteDuck(id));
    }
}
package com.pikecape.springbootrestapihibernate.repository;

import com.pikecape.springbootrestapihibernate.model.Duck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DuckRepository extends JpaRepository<Duck, UUID>
{
}

package com.pikecape.springbootrestapihibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRestApiHibernateApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(SpringbootRestApiHibernateApplication.class, args);
	}
}

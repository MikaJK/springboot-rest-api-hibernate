package com.pikecape.springbootrestapihibernate.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pikecape.springbootrestapihibernate.exceptions.ResourceNotFoundException;
import com.pikecape.springbootrestapihibernate.model.Duck;
import com.pikecape.springbootrestapihibernate.service.DuckService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@WebMvcTest(DuckController.class)
public class DuckControllerTest
{
    private UUID dueyId = UUID.randomUUID();
    private UUID hueyId = UUID.randomUUID();
    private UUID lueyId = UUID.randomUUID();
    private String dueyName = "Duey Duck";
    private String hueyName = "Huey Duck";
    private String lueyName = "Luey Duck";
    private Duck duey = new Duck(this.dueyId, this.dueyName);
    private Duck huey = new Duck(this.hueyId, this.hueyName);
    private Duck luey = new Duck(this.lueyId, this.lueyName);
    private List<Duck> ducks = Arrays.asList(new Duck[]{duey, huey, luey});
    private String baseUrl = "/api/v1/ducks";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DuckService duckService;

    private JacksonTester<Duck> jsonTester;

    private Duck duck;

    @Before
    public void setup() {
        JacksonTester.initFields(this, this.objectMapper);
        this.duck = new Duck();
    }

    @Test
    public void testHttpGet200() throws Exception {
        Mockito.when(duckService.getDuck(this.dueyId)).thenReturn(this.duey);
        this.mockMvc.perform(MockMvcRequestBuilders.get(this.baseUrl + "/" + this.dueyId))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(this.duey.toString()));
        //.string(Matchers.containsString("Hello Mock")));
    }

    @Test
    public void testHttpGet404() throws Exception {
        UUID unknownId = UUID.randomUUID();
        Mockito.when(duckService.getDuck(unknownId)).thenThrow(new ResourceNotFoundException(""));
        //.thenReturn(this.duey);
        this.mockMvc.perform(MockMvcRequestBuilders.get(this.baseUrl + "/" + unknownId))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testHttpGetAll200() throws Exception {
        Mockito.when(duckService.getAllDucks()).thenReturn(this.ducks);

        String expResult = "[" + this.duey.toString() + "," + this.huey.toString() + "," + this.luey.toString() + "]";

        this.mockMvc.perform(MockMvcRequestBuilders.get(this.baseUrl))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(expResult));
    }

    @Test
    public void testHttpPost201() throws Exception {
        Duck newDuck = new Duck();
        newDuck.setName("Duey Duck");

        final String duckJson = jsonTester.write(this.duck).getJson();

        Mockito.when(duckService.createDuck(Mockito.any(Duck.class))).thenReturn(this.duey);

        this.mockMvc.perform(MockMvcRequestBuilders.post(this.baseUrl).contentType(MediaType.APPLICATION_JSON).content(duckJson))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(this.duey.toString()));
    }

    @Test
    public void testHttpPut200() throws Exception {
        final String duckJson = jsonTester.write(this.duck).getJson();

        Mockito.when(duckService.updateDuck(Mockito.any(UUID.class), Mockito.any(Duck.class))).thenReturn(this.duey);

        this.mockMvc.perform(MockMvcRequestBuilders.put(this.baseUrl + "/" + this.dueyId).contentType(MediaType.APPLICATION_JSON).content(duckJson))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(this.duey.toString()));
    }

    @Test
    public void testHttpPut404() throws Exception {
        UUID unknownId = UUID.randomUUID();
        final String duckJson = jsonTester.write(this.duck).getJson();

        Mockito.when(duckService.updateDuck(Mockito.any(UUID.class), Mockito.any(Duck.class))).thenThrow(new ResourceNotFoundException(""));

        this.mockMvc.perform(MockMvcRequestBuilders.put(this.baseUrl + "/" + unknownId).contentType(MediaType.APPLICATION_JSON).content(duckJson))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testHttpDelete200() throws Exception {
        Mockito.when(duckService.deleteDuck(Mockito.any(UUID.class))).thenReturn(true);

        this.mockMvc.perform(MockMvcRequestBuilders.delete(this.baseUrl + "/" + this.dueyId))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testHttpDelete404() throws Exception {
        UUID unknownId = UUID.randomUUID();

        Mockito.when(duckService.deleteDuck(Mockito.any(UUID.class))).thenThrow(new ResourceNotFoundException(""));

        this.mockMvc.perform(MockMvcRequestBuilders.delete(this.baseUrl + "/" + unknownId))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
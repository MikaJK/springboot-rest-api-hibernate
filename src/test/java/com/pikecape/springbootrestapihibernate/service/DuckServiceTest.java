package com.pikecape.springbootrestapihibernate.service;

import com.pikecape.springbootrestapihibernate.exceptions.ResourceNotFoundException;
import com.pikecape.springbootrestapihibernate.model.Duck;
import com.pikecape.springbootrestapihibernate.repository.DuckRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@WebMvcTest(DuckRepository.class)
class DuckServiceTest
{
    private UUID dueyId = UUID.randomUUID();
    private UUID hueyId = UUID.randomUUID();
    private UUID lueyId = UUID.randomUUID();
    private String dueyName = "Duey Duck";
    private String hueyName = "Huey Duck";
    private String lueyName = "Luey Duck";
    private Duck duey = new Duck(this.dueyId, this.dueyName);
    private Duck huey = new Duck(this.hueyId, this.hueyName);
    private Duck luey = new Duck(this.lueyId, this.lueyName);
    private List<Duck> ducks = Arrays.asList(new Duck[]{duey, huey, luey});

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DuckRepository duckRepository;

    @Test
    void getAllDucks() {

        Mockito.when(duckRepository.findAll()).thenReturn(this.ducks);

        DuckService duckService = new DuckService(this.duckRepository);

        List<Duck> result = duckService.getAllDucks();
        assertNotNull(result);
        assertEquals(this.ducks, result);
    }

    @Test
    void getDuck() throws ResourceNotFoundException {
        Mockito.when(duckRepository.findById(this.dueyId)).thenReturn(java.util.Optional.ofNullable(this.duey));

        DuckService duckService = new DuckService(this.duckRepository);

        Duck result = duckService.getDuck(this.dueyId);
        assertNotNull(result);
        assertEquals(this.dueyId, result.getId());
        assertEquals(this.dueyName, result.getName());
    }

    @Test
    void createDuck() {
    }

    @Test
    void updateDuck() {
    }

    @Test
    void deleteDuck() {
    }
}